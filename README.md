# dl-ubuntu-docker

Builds a docker image which uses `docker` inside an Ubuntu base image.

## Purpose

This docker container can be used in a CI/CD pipeline inplace of the `docker/docker` image. It is meant to provide an Ubuntu environment instead of the alpine environment that is provided by the `docker/docker` image.

## Usage

In a GitLab environment:

```
docker run -v certs:/certs/client registry.gitlab.com/ragingpastry/dl-ubuntu-docker:latest
```

## Features

### Distribution

The image is based off of the ubuntu base image
